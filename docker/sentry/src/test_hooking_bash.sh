#!/usr/bin/env bash
#https://stackoverflow.com/questions/13468481/when-to-use-set-e/13478622#13478622
#no topo de cada bashscript.
#
#-ufaz com que seja um erro fazer referência a uma variável de ambiente não-existente, como ${HSOTNAME}, ao custo de exigir alguma ginástica com a verificação ${#}antes de referência ${1}, ${2}e assim por diante.
#
#pipefailfaz coisas como misspeled-command | sed -e 's/^WARNING: //'aumentar erros.
set -e
#set -eu
#set -o pipefail
#Hooking Bash
#https://blog.sentry.io/2017/11/28/sentry-bash

export SENTRY_DSN=http://6ee5afe1fc7f48b194d85b5aa13e8bae@sentry:9000/2
eval "$(sentry-cli bash-hook)"

asdasd