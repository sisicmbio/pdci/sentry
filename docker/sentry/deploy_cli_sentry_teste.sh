#!/usr/bin/env bash
rm -rf ./.env
set -ex

usage()
{
    echo "======================================================================================="
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -dev | dev                  Ambiente local de desenvolvimento Colaborativo da Tecnologia da Informacao"
    echo "      -tcti | tcti               Ambiente de Teste Colaborativo da Tecnologia da Informacao"
    echo "      -hmg  | hmg                Ambiente de Homologacao "
    echo "      -prd  | prd                Ambiente de Produção (Usúario Final)"
    echo "======================================================================================="
}

case $1 in
    -dev| dev )
     export PDCI_FILE_ENV_DOCKER=./.env.docker.dev.sentry

       __pdci_docker_host=tcp://127.0.0.1:2375
       __pdci_application_env=dev
       export DOCKER_COMPOSE_FILE=sentry_docker-compose.yml
       export PDCI_APPLICATION_ENV=${__pdci_application_env}
    ;;
    -tcti| tcti )
     export PDCI_FILE_ENV_DOCKER=./.env.docker.tcti

       __pdci_docker_host=tcp://10.197.93.75:2375
       __pdci_application_env=pdci_tcti
       export DOCKER_COMPOSE_FILE=docker-compose.yml
       export PDCI_APPLICATION_ENV=tcti
    ;;
    -prd | prd )
      export PDCI_FILE_ENV_DOCKER=./.env.docker.prd
       __pdci_docker_host=tcp://10.197.32.76:2375
       __pdci_application_env=pdci_production
       export DOCKER_COMPOSE_FILE=docker-compose.yml
       export PDCI_APPLICATION_ENV=production
    ;;
    -hmg | hmg )
      export PDCI_FILE_ENV_DOCKER=./.env.docker.hmg
       __pdci_docker_host=tcp://10.197.94.75:2375
       __pdci_application_env=pdci_homologacao
       export DOCKER_COMPOSE_FILE=docker-compose.yml
       export PDCI_APPLICATION_ENV=homologacao
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)



if [ "${__pdci_docker_host}" == "" ]; then

    echo "ERRO :: Ambiente nao foi informado"
    usage
    exit 1
fi

verifica_instalacao_docker()
{
existe=$(which docker)

if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER NAO ESTA INSTALADO"
    exit 1
fi

existe=$(which docker-compose)
if [ "${existe}" == "" ]; then
    echo "ERRO :: DOCKER COMPOSER NAO ESTA INSTALADO"
    exit 1
fi

}

#SENTRY_SERVER_EMAIL
#The email address used for From: in outbound emails. Default: root@localhost
#
#SENTRY_EMAIL_HOST, SENTRY_EMAIL_PORT, SENTRY_EMAIL_USER, SENTRY_EMAIL_PASSWORD, SENTRY_EMAIL_USE_TLS
#Connection information for an outbound smtp server. These values aren't needed if a linked smtp container exists.

#docker -H ${__pdci_docker_host} volume create data_sentry_pgdata
#docker -H ${__pdci_docker_host} volume create data_sentry_conf
#docker -H ${__pdci_docker_host} volume create data_sentry_files
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-redis redis
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped -v data_sentry_conf:/var/lib/postgresql/data  --name sentry-postgres -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=sentry postgres
#
#
#docker -H ${__pdci_docker_host} run -it --rm -m 2048m --cpu-shares=512 --name sentry-upgrade -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry upgrade
#
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name my-sentry -v data_sentry_files:/var/lib/sentry/files -e SENTRY_SERVER_EMAIL=redmine-projetos@icmbio.gov.br -e SENTRY_EMAIL_HOST=mail.icmbio.gov.br -p 9009:9000 -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-redis:redis --link sentry-postgres:postgres sentry
#
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-cron -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry run cron
#docker -H ${__pdci_docker_host} run -d -m 1024m --cpu-shares=512  --restart unless-stopped --name sentry-worker-1 -e SENTRY_SECRET_KEY='d862-e89gf!jf49^fj410ps#z5%afei=2=j#z4pr(g1x^frdbr' --link sentry-postgres:postgres --link sentry-redis:redis sentry run worker


#Instalacao com docker composer

# TODO FAZER COM QUE O SCRIPT DESCUBRA QUAIS SAO OS SERVICOS QUE VEEM DO REGISTRY E APENAS NESTES REALIZE O PULL
docker-compose  -H ${__pdci_docker_host}  -p ${__pdci_application_env} -f  ${DOCKER_COMPOSE_FILE}  pull sentry_test
echo ""
echo "Removendo containers do pdci suite ..."
echo ""
docker-compose  -H ${__pdci_docker_host} -p ${__pdci_application_env} -f  ${DOCKER_COMPOSE_FILE} rm -f -s -v sentry_test


docker-compose  -H ${__pdci_docker_host}  -p ${__pdci_application_env} -f ${DOCKER_COMPOSE_FILE} config

docker-compose -H ${__pdci_docker_host}  -p ${__pdci_application_env} -f ${DOCKER_COMPOSE_FILE}  run --name dev_sentry_test_run --service-ports sentry_test

