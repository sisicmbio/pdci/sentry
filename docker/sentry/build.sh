#!/usr/bin/env bash

set +xe

CI_REGISTRY=registry.gitlab.com

docker logout

docker login ${CI_REGISTRY}

#docker build --no-cache  --pull --build-arg PDCI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg PDCI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} --build-arg PDCI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} --build-arg PDCI_COMMIT_TAG=${CI_COMMIT_TAG}  --target sql -t "${CI_REGISTRY}/sisicmbio/pdci/sentry:develop" .
docker build --pull --build-arg PDCI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg PDCI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} --build-arg PDCI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} --build-arg PDCI_COMMIT_TAG=${CI_COMMIT_TAG}  --target sql -t "${CI_REGISTRY}/sisicmbio/pdci/sentry:develop" .

docker push  "${CI_REGISTRY}/sisicmbio/pdci/sentry:develop"

