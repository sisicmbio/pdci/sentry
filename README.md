# sentry

## Visão 
Desenvolver um sistema web para atenteder a necessidades do [...] de acordo com as metodologias especificadas pelo Instituto Chico Mendes de Biodiversidade (ICMBio).

## Objetivo do Sistema

Objetivo 1
Objetivo 2


## Instalação ambiente de desenvolvimento

### Pré-requisitos:
Ter instalado o docker e docker-compose na máquina.

Construção de ambiente de desenvolvimento alinhado às normas do PDCI do ICMBio.
#### Opção 1 - Utilizando os scripts de deploys

1) Subindo container com banco de dados no DEV

De permissão de execução no arquivo de deploy do DB:

     ```
     chmod +x deploy_dev__db_docker-compose.sh
     ```
     
Execute o script de deploy e siga as intruções.     
          ```
          ./deploy_dev__db_docker-compose.sh dev
          ```

2) Subindo suite da aplicação no DEV

De permissão de execução no arquivo de deploy da app:

     ```
     chmod +x deploy_dev__docker-compose.sh
 
     ```
     
Execute o script de deploy e siga as intruções.     
          ```
          ./deploy_dev__docker-compose.sh dev
          ```     

Neste ponto vc deve ter os containers rodando em sua máquina.

Importante configurar o seu arquivo `/etc/hosts` com as seguintes linhas (substituindo pelo seu IP)

```
192.168.1.140 sicae.sentry.sisicmbio.icmbio.gov.br
192.168.1.140 dev.sentry.sisicmbio.icmbio.gov.br
```

Pode acessar o http://dev.sentry.sisicmbio.icmbio.gov.br para verificar se a aplicação com a última imagem publicada está rodando.

          
#### Deploy em TCTI

Acessar o jenkins em jenkins.icmbio.gov.br

Acesse a aba do projeto sentry

#### Deploy em HMG

#### Deploy em PRD