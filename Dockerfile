#FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
#ENV APP_PROJETO "SentryTeste"
#COPY sql  /sql/
#ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

############################################################################
##Stage pg_alpine
FROM postgres:10.12-alpine AS pg_alpine
RUN mkdir -p /dump
COPY dump /dump
COPY docker-entrypoint-initdb.d /docker-entrypoint-initdb.d

RUN chmod +x /docker-entrypoint-initdb.d/*.sh -R



############################################################################
##Stage develop nova versão
FROM registry.gitlab.com/sisicmbio/pdci/sentry:v1-5-0-release AS develop

COPY src/ /

RUN chmod +x /entrypoint.sh
    # apt-get update && \
    # apt-get install -y --no-install-recommends gettext-base && \
    # apt-get clean && \
    # rm -rf /var/lib/apt/lists/* && \

#RUN  pip install django-auth-ldap



############################################################################
##Stage onbuild
FROM sentry:9.1.2-onbuild AS onbuild-develop

################################################################################
##Stage build
FROM develop AS build

###############################################################################
##Stage develop
FROM develop AS production

######################################################################################
##Stage testing
FROM develop AS testing
#