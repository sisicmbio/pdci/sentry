#!/usr/bin/env bash

set -ex

echo "${__pdci_registry_gitlab_deploy_token_password}" | docker  -H ${__PDCI_DOCKER_HOST_TCTI} login --username ${__pdci_registry_gitlab_deploy_token_username} --password-stdin  ${__PDCI_REGISTRY}

PDCI_BD_PORT=$(docker  -H 10.197.32.76   inspect --format='{{(index (index .NetworkSettings.Ports "5432/tcp") 0).HostPort}}' pdci_production_sentry_db_1)

pg_dump -U postgres -h 10.197.32.76 -p ${PDCI_BD_PORT} -w  --disable-triggers -d sentry > ${WORKSPACE}/dump/001-sentry-dump-prd.sql

docker -H ${__PDCI_DOCKER_HOST} build  --no-cache --pull --target pg_alpine -t "registry.gitlab.com/sisicmbio/pdci/sentry/postgres:10.12-alpine" .
docker -H ${__PDCI_DOCKER_HOST} push "registry.gitlab.com/sisicmbio/pdci/sentry/postgres:10.12-alpine"
docker -H ${__PDCI_DOCKER_HOST} rmi "registry.gitlab.com/sisicmbio/pdci/sentry/postgres:10.12-alpine"