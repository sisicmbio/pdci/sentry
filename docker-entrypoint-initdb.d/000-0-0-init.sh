#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'sentry';
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    DROP DATABASE IF EXISTS sentry;

EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    DO
    \$do$
        DECLARE r record;
        BEGIN

                IF NOT EXISTS(
                        SELECT -- SELECT list can stay empty for this
                        FROM   pg_catalog.pg_roles
                        WHERE  rolname = 'usr_sentry') THEN

                CREATE ROLE usr_sentry LOGIN ENCRYPTED PASSWORD 'md5d6ae13205d2b66b00160d41252b444af'
                  SUPERUSER CREATEDB CREATEROLE REPLICATION
                   VALID UNTIL 'infinity';

                END IF;


        END;
    \$do$;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
      CREATE DATABASE sentry
                  WITH OWNER = usr_sentry
                  ENCODING = 'UTF8';
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname sentry <<-EOSQL
      CREATE SCHEMA IF NOT EXISTS sentry AUTHORIZATION usr_sentry;
      CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA sentry;
      ALTER DATABASE sentry SET search_path = sentry, public;
      GRANT CONNECT ON DATABASE sentry TO usr_sentry;
EOSQL

#ls -la /dump/001-sentry-dump-prd.sql


#sed -i "s|CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;|CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA sentry;|g" /dump/001-sentry-dump-prd.sql
#sed -i "s|public\.|sentry\.|g" /dump/001-sentry-dump-prd.sql
#sed -i "s|OWNER TO sentry|OWNER TO usr_sentry|g" /dump/001-sentry-dump-prd.sql

psql -v ON_ERROR_STOP=0 --username "$POSTGRES_USER" --dbname sentry < /dump/001-sentry-dump-prd.sql
